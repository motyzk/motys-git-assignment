import pathlib


def copy_files(folder):
    for file in folder.iterdir():
        if file.suffix == '.txt':
            out.write(file.read_text())
        elif file.is_dir():
            copy_files(file)


out = open('contents.txt', 'w')
copy_files(pathlib.Path(r'.'))

out.close()

